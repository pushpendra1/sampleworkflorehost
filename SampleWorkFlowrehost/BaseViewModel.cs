﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleWorkFlowrehost
{
    public class BaseViewModel : INotifyPropertyChanged, IDisposable
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void Dispose()
        {
            this.OnDispose();
        }

        protected virtual void OnPropertyChanged(string PropertyString)
        {
            PropertyChangedEventHandler _handler = this.PropertyChanged;
            if (_handler != null)
            {
                var Arg = new PropertyChangedEventArgs(PropertyString);
                _handler(this, Arg);
            }
        }

        protected virtual void OnDispose()
        {

        }
    }
}
