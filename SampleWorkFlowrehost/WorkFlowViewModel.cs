﻿using System;
using System.Activities.Core.Presentation;
using System.Activities.Presentation;
using System.Activities.Presentation.View;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SampleWorkFlowrehost
{
    public class WorkFlowViewModel : BaseViewModel
    {
        public WorkflowDesigner Designer;
        public MainWindowViewModel mainwindowviewmodel;
        public WorkFlowViewModel(MainWindowViewModel viewmodel)
        {
            this.mainwindowviewmodel = viewmodel;
            new DesignerMetadata().Register();
            this.Designer = new WorkflowDesigner();
            this.Designer.ModelChanged += delegate (object sender, EventArgs args)
            {
                this.OnPropertyChanged("DesignerConrol");
            };
            this.Designer.Context.Services.GetService<DesignerConfigurationService>().TargetFrameworkName = new System.Runtime.Versioning.FrameworkName(".NETFramework", new Version(4, 5));
            this.Designer.Context.Services.GetService<DesignerConfigurationService>().LoadingFromUntrustedSourceEnabled = true;
            this.Designer.Context.Services.Publish<IExpressionEditorService>(new EditorService(mainwindowviewmodel));
        }

        public UIElement DesignerView
        {
            get
            {
                return this.Designer.View;
            }
        }

        public UIElement OutlineView
        {
            get { return this.Designer.OutlineView; }
        }

        public UIElement PropertyInspector
        {
            get
            {
                return this.Designer.PropertyInspectorView;
            }
        }
    }
}
