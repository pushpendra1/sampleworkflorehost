﻿using RoslynPad.Editor;
using System;
using System.Activities.Presentation.View;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SampleWorkFlowrehost
{
    public class EditorInstance : IExpressionEditorInstance
    {
        public RoslynCodeEditor editor = null;
        private MainWindowViewModel _mainwindowiewmodel;
        public EditorInstance(MainWindowViewModel viewmodel, string previous)
        {
            editor = new RoslynCodeEditor();
            editor.ShowLineNumbers = false;
            this._mainwindowiewmodel = viewmodel;

            this.editor.Initialize(this._mainwindowiewmodel.Host, new ClassificationHighlightColors(),Directory.GetCurrentDirectory(), string.Empty);

            this.LostAggregateFocus += EditorInstance_LostAggregateFocus;
            editor.LostFocus += Editor_LostFocus;
        }

        private void Editor_LostFocus(object sender, RoutedEventArgs e)
        {
            //editor
        }

        private void EditorInstance_LostAggregateFocus(object sender, EventArgs e)
        {
            editor.ReleaseMouseCapture();
        }

        public Control HostControl
        {
            get
            {
                return editor;
            }
        }

        public string Text
        {
            get
            {
                return editor.Text;
            }
            set
            {
                editor.Text = (string)value;
            }
        }
        public ScrollBarVisibility VerticalScrollBarVisibility
        {
            get
            {
                return editor.VerticalScrollBarVisibility;
            }
            set
            {
                editor.VerticalScrollBarVisibility = (ScrollBarVisibility)value;
            }
        }
        public ScrollBarVisibility HorizontalScrollBarVisibility
        {
            get
            {
                return editor.HorizontalScrollBarVisibility;
            }
            set
            {
                editor.HorizontalScrollBarVisibility = (ScrollBarVisibility)value;
            }
        }
        public int MinLines { get; set; }
        public int MaxLines { get; set; }

        public bool HasAggregateFocus
        {
            get
            {
                return true;
            }
        }

        public bool AcceptsReturn { get; set; }
        public bool AcceptsTab { get; set; }

        public event EventHandler TextChanged;
        public event EventHandler LostAggregateFocus;
        public event EventHandler GotAggregateFocus;
        public event EventHandler Closing;

        public bool CanCompleteWord()
        {
            return true;
        }

        public bool CanCopy()
        {
            return true;
        }

        public bool CanCut()
        {
            return true;
        }

        public bool CanDecreaseFilterLevel()
        {
            return true;
        }

        public bool CanGlobalIntellisense()
        {
            return true;
        }

        public bool CanIncreaseFilterLevel()
        {
            return true;
        }

        public bool CanParameterInfo()
        {
            return true;
        }

        public bool CanPaste()
        {
            return true;
        }

        public bool CanQuickInfo()
        {
            return true;
        }

        public bool CanRedo()
        {
            return editor.CanRedo;
        }

        public bool CanUndo()
        {
            return editor.CanUndo;
        }

        public void ClearSelection()
        {

        }

        public void Close()
        {

        }

        public bool CompleteWord()
        {
            return true;
        }

        public bool Copy()
        {
            editor.Copy();
            return true;
        }

        public bool Cut()
        {
            editor.Cut();
            return true;
        }

        public bool DecreaseFilterLevel()
        {
            return true;
        }

        public void Focus()
        {
            editor.Focus();
        }

        public string GetCommittedText()
        {
            return this.Text;
        }

        public bool GlobalIntellisense()
        {
            return true;
        }

        public bool IncreaseFilterLevel()
        {
            return true;
        }

        public bool ParameterInfo()
        {
            return true;
        }

        public bool Paste()
        {
            editor.Paste();
            return true;
        }

        public bool QuickInfo()
        {
            return true;
        }

        public bool Redo()
        {
            return editor.Redo();
        }

        public bool Undo()
        {
            return editor.Undo();
        }

    }
}
