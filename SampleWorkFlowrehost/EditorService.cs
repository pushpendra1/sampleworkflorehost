﻿using System;
using System.Activities.Presentation.Hosting;
using System.Activities.Presentation.Model;
using System.Activities.Presentation.View;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SampleWorkFlowrehost
{
    public class EditorService : IExpressionEditorService
    {
        public MainWindowViewModel mainwindowViewModel;
        private EditorInstance editor;
        public EditorService(MainWindowViewModel _viewModel)
        {
            this.mainwindowViewModel = _viewModel;
        }
        public void CloseExpressionEditors()
        {
            throw new NotImplementedException();
        }

        public IExpressionEditorInstance CreateExpressionEditor(AssemblyContextControlItem assemblies, ImportedNamespaceContextItem importedNamespaces, List<ModelItem> variables, string text, Type expressionType)
        {
            return CreateExpressionEditorPrivate(assemblies, importedNamespaces, variables, text, null, null);
        }

        public IExpressionEditorInstance CreateExpressionEditor(AssemblyContextControlItem assemblies, ImportedNamespaceContextItem importedNamespaces, List<ModelItem> variables, string text, Type expressionType, Size initialSize)
        {
            return CreateExpressionEditorPrivate(assemblies, importedNamespaces, variables, text, expressionType, null/* TODO Change to default(_) if this is not a reference type */);
        }

        public IExpressionEditorInstance CreateExpressionEditor(AssemblyContextControlItem assemblies, ImportedNamespaceContextItem importedNamespaces, List<ModelItem> variables, string text)
        {
            return CreateExpressionEditorPrivate(assemblies, importedNamespaces, variables, text, null, null);
        }

        public IExpressionEditorInstance CreateExpressionEditor(AssemblyContextControlItem assemblies, ImportedNamespaceContextItem importedNamespaces, List<ModelItem> variables, string text, Size initialSize)
        {
            return CreateExpressionEditorPrivate(assemblies, importedNamespaces, variables, text, null/* TODO Change to default(_) if this is not a reference type */, initialSize);
        }

        public void UpdateContext(AssemblyContextControlItem assemblies, ImportedNamespaceContextItem importedNamespaces)
        {

        }

        private IExpressionEditorInstance CreateExpressionEditorPrivate(AssemblyContextControlItem assemblies, ImportedNamespaceContextItem importedNamespaces, List<ModelItem> variables, string text, object p1, object p2)
        {
            editor = new EditorInstance(this.mainwindowViewModel, text);
            return editor;
        }

    }
}
