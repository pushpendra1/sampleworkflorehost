﻿using RoslynPad.Roslyn;
using System;
using System.Activities.Presentation;
using System.Activities.Presentation.Toolbox;
using System.Activities.Statements;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SampleWorkFlowrehost
{
    public class MainWindowViewModel : BaseViewModel
    {
        private RoslynHost _host;
        public MainWindow MainWindowUI;
        public MainWindowViewModel(MainWindow window)
        {
            this.InitializeToolbox();
            this.MainWindowUI = window;
            this.MainWindowUI.Loaded += MainWindowUI_Loaded;
        }
        private void MainWindowUI_Loaded(object sender, RoutedEventArgs e)
        {
            this.MainWindowUI.Loaded -= MainWindowUI_Loaded;

            _host = new RoslynHost(additionalAssemblies: new[]
            {
                Assembly.Load("RoslynPad.Roslyn.Windows"),
                Assembly.Load("RoslynPad.Editor.Windows")
            });

            this.WFviewModel = new WorkFlowViewModel(this);
            this.WFviewModel.Designer.Load(Directory.GetCurrentDirectory()+@"\DefaultDesigner.xaml");
            this.Refresh();
        }

        public RoslynHost Host
        {
            get
            {
                return this._host;
            }
        }

        #region PropertyInspector ane Outline
        public object PropertyInspector
        {
            get
            {
                if (this.WFviewModel != null)
                {
                    return this.WFviewModel.PropertyInspector;
                }
                return null;
            }
            set { this.OnPropertyChanged("PropertyInspector"); }
        }

        public object OutlineView
        {
            get
            {
                if (this.WFviewModel != null)
                {
                    return this.WFviewModel.OutlineView;
                }
                return null;
            }
            set { this.OnPropertyChanged("OutlineView"); }
        }
        #endregion

        #region ToolBox
        private ToolboxControl ToolBox;

        private Dictionary<ToolboxCategory, IList<string>> ToolboxActivities { get; set; }

        private Dictionary<string, ToolboxCategory> toolboxCategory;
        public ToolboxControl TBcontrol
        {
            get { return this.ToolBox; }
            set
            {
                this.OnPropertyChanged("TBcontrol");
            }
        }

        private void InitializeToolbox()
        {
            this.ToolBox = new ToolboxControl();
            this.ToolboxActivities = new Dictionary<ToolboxCategory, IList<string>>();
            this.toolboxCategory = new Dictionary<string, ToolboxCategory>();
            this.AddCategoriesNactivitie(
                new List<Type>
                {
                    //ControlFlow
                    typeof(Assign),typeof(DoWhile),typeof(Delay),typeof(Flowchart), typeof(FlowDecision),
                    typeof(FlowSwitch<>),typeof(If), typeof(Switch<>), typeof(Sequence)
                    ,typeof(While),typeof(WriteLine),

                    typeof(AddToCollection<>),typeof(ClearCollection<>),typeof(ExistsInCollection<>),typeof(RemoveFromCollection<>)

                });

        }

        private void AddCategoriesNactivitie(List<Type> Activitylist)
        {
            foreach (Type Activity in Activitylist)
            {
                if (IsValidActivity(Activity))
                {
                    ToolboxCategory cat = this.GetToolboxCategory(Activity.Namespace);

                    if (!this.ToolboxActivities[cat].Contains(Activity.FullName))
                    {
                        this.ToolboxActivities[cat].Add(Activity.FullName);
                        cat.Add(new ToolboxItemWrapper(Activity.FullName, Activity.Assembly.FullName, null, Activity.Name));
                    }
                }
            }
        }

        private bool IsValidActivity(Type activity)
        {
            return activity.IsPublic && !activity.IsNested && !activity.IsAbstract
                && (typeof(System.Activities.Activity).IsAssignableFrom(activity) || typeof(IActivityTemplateFactory).IsAssignableFrom(activity) || typeof(FlowNode).IsAssignableFrom(activity));
        }

        private ToolboxCategory GetToolboxCategory(string name)
        {
            if (this.toolboxCategory.ContainsKey(name))
            {
                return this.toolboxCategory[name];
            }
            else
            {
                ToolboxCategory category = new ToolboxCategory(name);
                this.toolboxCategory[name] = category;
                this.ToolboxActivities.Add(category, new List<string>());

                this.ToolBox.Categories.Add(category);
                return category;
            }
        }
        #endregion

        #region DesignerView


        private WorkFlowViewModel WFviewModel;
        public object DesignerConrol
        {
            get
            {
                if (this.WFviewModel != null)
                {
                    return this.WFviewModel.DesignerView;
                }
                return null;
            }
            set
            {

                this.OnPropertyChanged("DesignerConrol");
            }
        }
        #endregion
        public void Refresh()
        {
            this.OnPropertyChanged("DesignerConrol");
            this.OnPropertyChanged("PropertyInspector");
            this.OnPropertyChanged("OutlineView");
        }
    }
}
